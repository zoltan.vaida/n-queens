
#include<stdio.h> 
#include<stdbool.h> 
#include <vector>

using namespace std;

bool nQueen(int **table, int col, int n);
bool isOK(int **table, int, int, int);
void printTable(int **table, int n);
bool nQueenForwardChecking(int **table, int col, int n);
bool fc(int **table, int row, int col, int n);
std::vector<int> getRowsPropositions(int **table, int col, int n);
std::vector<int> getUnassignedFromConstraint(int **table, int col, int n);

int valueAssignment = 0;

int main()
{

	printf("Adja meg a kiralynok szamat: ");
	int n;
	scanf_s("%i", &n);

	printf("\nModszerek:\n1.Nyers Backtracking\n2.Backtracking + MVR + Forward Checking\n3. Backtracking + MVR + AC-3\n");
	printf("Melyik metodussal szeretne megoldani a feladatot?: ");
	int f;
	scanf_s("%i", &f);

	int** table = new int*[n];
	for (int i = 0; i < n; i++)
	{
		table[i] = new int[n];
	}

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			table[i][j] = 0;
		}
	}

	switch (f)
	{
	case 1:
		printf("n queen:\n");
		if (nQueen(table, 0, n) == false)
		{
			printf("Nincs megoldás");
			return -1;
		}
		printTable(table, n);
		printf("Ertekadasok szama: %i\n", valueAssignment);
		valueAssignment = 0;

		return 0;
	case 2:
		printf("n queen with forward checking:\n");
		if (nQueenForwardChecking(table, 0, n) == false)
		{
			printf("Nincs megoldás");
			return -1;
		}
		printTable(table, n);
		printf("Ertekadasok szama: %i\n", valueAssignment);
		valueAssignment = 0;

		return 0;
	case 3:
		printf("n queen with AC-3:\n");

		return 0;
	default:
		printf("Kerem 1 es 3 kozotti szamot adjon meg!\n");
		return 0;
	}
	

	delete table;
	
	return 0;
}

void printTable(int **table, int n)
{
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			printf(" %d ", table[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}


bool isOK(int **table, int row, int col, int n)
{
	int i, j;

	for (i = 0; i < col; i++)	//a sorban
	{
		if (table[row][i] == 1)
		{
			return false;
		}
	}

	for (i = 0; i < col; i++)	//az oszlopban
	{
		if (table[i][col] == 1)
		{
			return false;
		}
	}


	for (i = row, j = col; i >= 0 && j >= 0; i--, j--)	//fölötte az átló
	{
		if (table[i][j] == 1)
		{
			return false;
		}
	}

	for (i = row, j = col; j >= 0 && i < n; i++, j--)	//alatta az átló
	{
		if (table[i][j] == 1)
		{
			return false;
		}
	}

	return true;
}

bool nQueen(int **table, int col, int n)
{
	if (col >= n)
	{
		return true;	//megvan a megoldás
	}


	for (int i = 0; i < n; i++)
	{
		if (isOK(table, i, col, n))//ha tudja tenni
		{

			table[i][col] = 1;
			valueAssignment++;
			printf("Beraktam 1-est:\n");
			printTable(table, n);


			if (nQueen(table, col + 1, n))
			{
				return true;
			}


			table[i][col] = 0;//ha eljut ide, nem vezetett a megoldáshoz, ismét 0 lesz (kiveszi)
			printf("Kviettem 1-est:\n");
			printTable(table, n);
		}
	}

	return false;//nincs megoldás
}

bool nQueenForwardChecking(int **table, int col, int n)
{
	if (col >= n)
	{
		return true;	//megvan a megoldás
	}

	std::vector<int> rowPropositions = getRowsPropositions(table, col, n);
	for (int row : rowPropositions)
	{

		table[row][col] = 1;
		valueAssignment++;

		bool domainWipeOut = false;

		std::vector<int> unasignedFromConstraint = getUnassignedFromConstraint(table, col, n);
		int size = unasignedFromConstraint.size();
		for (int i = 0; i < size; i+2)
		{
			if (fc(table, unasignedFromConstraint[i], unasignedFromConstraint[i+1], n))
			{
				domainWipeOut = true;
				break;
			}
		}

		if (domainWipeOut == false)
		{
			if (nQueen(table, col + 1, n))
			{
				return true;
			}
		}

		table[row][col] = 0;
	}

	return false;//nincs megoldás
}

std::vector<int> getUnassignedFromConstraint(int **table, int col, int n)
{
	std::vector<int> result;

	for (int i = 0; i < n; i++)
	{
		for(int j = col +1; j < n; j ++)
		if (table[i][j] == 0 && isOK(table, i, j, n))
		{
			result.push_back(i);
			result.push_back(j);
		}
	}

	return result;
}

bool fc(int **table, int row, int col, int n)
{
	std::vector<int> actualDomain = getRowsPropositions(table, col, n);
	std::vector<int> tempDomain;
	tempDomain = actualDomain;

	for(int propositionRow : actualDomain)
	{
		if (!isOK(table, propositionRow, col, n))
		{
			tempDomain.erase(std::find(tempDomain.begin(), tempDomain.end(), propositionRow));
		}
	}

	int size = tempDomain.size();

	if (size == 0)
	{
		return true;
	}

	return false;
}

std::vector<int> getRowsPropositions(int **table, int col, int n)
{
	std::vector<int> rows;

	for (int i = 0; i < n; i++)
	{
		if (isOK(table, i, col, n))
		{
			rows.push_back(i);
		}
	}

	return rows;
}